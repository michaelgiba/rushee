Rushee
======

Light and simple auth ui starter for IOS


##About

Rushee is a project I started for the sake of making it easier to start any ios application 
that requires a log in because the most frustrating part of development in my opinion was the
redundancy of the log in portion, sign up portion and etc.

This project aims to add simple and tweakable template for starting projects of this type.
I also added some basic animation and styles to the screen just to make it look pretty.

Special Thanks to dependency projects of:

FlatUI
https://github.com/designmodo/Flat-UI

AFNetworking
https://github.com/AFNetworking/AFNetworking

and possibly more to come.


##Seamless transitions to each state
![alt tag](https://raw.github.com/michaelgiba/Rushee/master/Rushee/Screenshots/iOS%20Simulator%20Screen%20shot%20Mar%201%2C%202014%2C%202.59.38%20PM.png)
![alt tag](https://raw.github.com/michaelgiba/Rushee/master/Rushee/Screenshots/iOS%20Simulator%20Screen%20shot%20Mar%201%2C%202014%2C%202.59.40%20PM.png)
![alt tag](https://raw.github.com/michaelgiba/Rushee/master/Rushee/Screenshots/iOS%20Simulator%20Screen%20shot%20Mar%201%2C%202014%2C%202.59.43%20PM.png)
![alt tag](https://raw.github.com/michaelgiba/Rushee/master/Rushee/Screenshots/iOS%20Simulator%20Screen%20shot%20Mar%201%2C%202014%2C%202.59.48%20PM.png)




=======
##Contributors

If you would like to contribute to project in anyways please dont hesitate to issue a pull request or post.


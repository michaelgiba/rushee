//
//  SignInViewController.m
//  Rushee
//
//  Created by Michael Giba on 2/24/14.
//  Copyright (c) 2014 Michael Giba. All rights reserved.
//

#import "SignInViewController.h"
#import "UIColor+FlatUI.h"
#import "UIFont+FlatUI.h"
#import "AppDelegate.h"
#import <GoogleOpenSource/GoogleOpenSource.h>


@interface SignInViewController (){
    CGRect defaultGoogle;
    CGRect defaultFacebook;
    CGRect defaultUsername;
    CGRect defaultPassword;
    CGRect defaultEmail;
    CGRect defaultTopframe;
    CGRect defaultSignIn;
    CGRect defaultSignUp;
    UILabel *emailL;
    
}
@end

typedef enum {
    SIGN_IN = 0,
    SIGN_UP = 1,
    FORGOT = 2
} nativeState;

typedef enum {
    facebook = 1,
    google = 2,
    native = 3,
} socialState;


nativeState currentState = SIGN_IN;




@implementation SignInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{

    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    self.view.backgroundColor = [UIColor cloudsColor];


    if([self checkForAuth])
        [self setUpView];
    
    

}


//<------------------- Check For Previous Auth --------------->

-(BOOL)checkForAuth{
    
    
    bool facebookHasLoggedIn = false;
    facebookHasLoggedIn = [self checkForFacebook];
    

    if(!facebookHasLoggedIn){
        
        GPPSignIn *signIn = [GPPSignIn sharedInstance];
        signIn.shouldFetchGoogleUserEmail = YES;
        signIn.shouldFetchGoogleUserID = YES;
        signIn.shouldFetchGooglePlusUser = YES;
        signIn.clientID = googleClientID;
        signIn.scopes = @[ kGTLAuthScopePlusLogin];
        signIn.delegate = self;
     //  [signIn trySilentAuthentication];
        return true;
    }
    else{
        return false;
    }
    
    return false;
}





//<-------------------- Set Up View ----------------------->

-(void)setUpView{
    
    _nativeSignInButton.buttonColor = [UIColor amethystColor];
    _nativeSignInButton.cornerRadius = 2.0f;
    _nativeSignUpButton.titleLabel.text = @"Sign In";
    [_nativeSignInButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [_nativeSignInButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateHighlighted];
    
    
    _usernameTF.delegate = self;
    _passwordTF.delegate = self;
    _emailField.delegate = self;
    
    emailL.text = @"Enter your email address and well send you a message";
    emailL = [[UILabel alloc] initWithFrame:CGRectMake(35, 30, 250, 230)];
    emailL.numberOfLines = 2;
    emailL.textAlignment = NSTextAlignmentCenter;
    emailL.hidden = YES;
    
    
    [self.view addSubview:emailL];
    
    
    CGRect usr = _usernameTF.frame;
    CGRect psd = _passwordTF.frame;
    CGRect eml = _emailField.frame;
    
    
    defaultTopframe = _TopSlidePanel.frame;
    defaultGoogle = _googleSignUpButton.frame;
    defaultFacebook =_facebookSignUpButton.frame;
    defaultSignUp = _nativeSignUpButton.frame;
    defaultSignIn = _nativeSignInButton.frame;
    
    
    usr.size.height = usr.size.height + 10;
    psd.size.height = psd.size.height + 10;
    eml.size.height = eml.size.height + 10;
    
    defaultUsername = usr;
    defaultPassword = psd;
    defaultEmail = eml;
    
    
    _usernameTF.frame = usr;
    _passwordTF.frame = psd;
    _emailField.frame = eml;
    
    
    _usernameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _passwordTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    
    _passwordTF.secureTextEntry = YES;
    
}






//<----------- Native Sign Up and Sign In ------------------>

- (IBAction)buttonPushed:(id)sender {
    
}


//<------------- Open Auth Log in -------------------->



- (BOOL)checkForFacebook {

    if ([FBSession activeSession].isOpen){

        [self signUpCompleteFadeOutWithComepltion:nil];

        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection,NSDictionary<FBGraphUser>*user,NSError *error){
             if(error){
                 [self defaultEverything];
                 [[FBSession activeSession] closeAndClearTokenInformation];
             }
             else{
                 NSString *username = [@"FB_USER_" stringByAppendingString:user[@"username"]];
                 NSDictionary *dict = @{
                            @"Email":user[@"email"],
                            @"Name":user[@"name"],
                            @"Username":username
                            };
                 
            
                 [self handleUser:dict socialDataType:facebook];
             }
        }];
        return true;
    }
    else {

        FBSession *session = [[FBSession alloc]initWithPermissions:@[@"email"]];
        [FBSession setActiveSession:session];
        if ([FBSession activeSession].state == FBSessionStateCreatedTokenLoaded) {
            [FBSession openActiveSessionWithReadPermissions:@[@"email"]
                                               allowLoginUI:YES
                                          completionHandler:^(FBSession *session,FBSessionState state, NSError *error){
                                              
                                              [self checkForFacebook];
                                          }];
            
        }
        return false;
    }

};





- (IBAction)facebookPressed:(id)sender {


    
    if ([FBSession activeSession].state != FBSessionStateCreated)
        [FBSession setActiveSession: [[FBSession alloc] init]];
    
    [self checkForFacebook];
    
    [FBSession openActiveSessionWithReadPermissions:@[@"email"] allowLoginUI:YES completionHandler:^(FBSession *sesson, FBSessionState state, NSError *error){
        if(!error) [self checkForFacebook];
        else NSLog(@"%@",error);
    }];
    
    
}



- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth  error: (NSError *) error{
    if(!error){
        

        GTLServicePlus* plusService = [[GTLServicePlus alloc] init] ;
        plusService.retryEnabled = YES;
        [plusService setAuthorizer:[GPPSignIn sharedInstance].authentication];
        
        GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
        plusService.apiVersion = @"v1";
        [plusService executeQuery:query
                completionHandler:^(GTLServiceTicket *ticket,
                                    GTLPlusPerson *person,
                                    NSError *error) {
                    if (error) {
#warning do some error stuff
                    }
                    else{
                        
                        NSString *username = [@"GP_USER_" stringByAppendingString:[GPPSignIn sharedInstance].userID];
                        NSDictionary *userData = @{
                                @"Email": [GPPSignIn sharedInstance].userEmail,
                                @"Name": person.displayName,
                                @"Username":username
                                };
                        

                        [self handleUser:userData socialDataType:google];
                    }
                }];
    

    }
    else{
#warning do some error thing
    }
}


-(void)handleUser:(NSDictionary *)auth socialDataType:(socialState)state{
    
    
    if(state == facebook || state == google){
        
    }
    
    
    [self signUpCompleteFadeOutWithComepltion:^(BOOL check){
        [self performSegueWithIdentifier:@"Welcome" sender:nil];
    }];
    
}

//<------------ Back Button ------------->

- (IBAction)backPushed:(id)sender {
    currentState = SIGN_IN;
    [self defaultEverything];
}


//<----------Send Email to Dispatch Url------->

- (IBAction)forgotPressed:(id)sender {
    [self morphViewForForgot];
}

//<----------Change to Sign in view----------->


- (IBAction)signUpPressed:(id)sender {

    [self morphViewForSignUp];
    
}






//<----------------Animations--------------------->


-(void)signUpCompleteFadeOutWithComepltion:(void (^)(BOOL))callback{
    
    [UIView animateWithDuration:.3f animations:^{
        CGRect googleFrame = _googleSignUpButton.frame;
        CGRect top = _TopSlidePanel.frame;
        CGRect fbFrame = _facebookSignUpButton.frame;
        _googleSignUpButton.frame = CGRectMake(googleFrame.origin.x, googleFrame.origin.y+1000,googleFrame.size.width,googleFrame.size.height);
        _facebookSignUpButton.frame = CGRectMake(fbFrame.origin.x, fbFrame.origin.y+1000, fbFrame.size.width, fbFrame.size.height);
        _nativeSignInButton.alpha = 0.0f;
        _nativeSignUpButton.alpha = 0.0f;
        _usernameTF.alpha = 0.0f;
        _passwordTF.alpha = 0.0f;
        _forgotPassword.alpha = 0.0f;
        _emailField.alpha = 0.0f;
        _TopSlidePanel.frame = CGRectMake(top.origin.x, top.origin.y-100, top.size.width, top.size.height);
    } completion:callback];
    
}



-(void)morphViewForSignUp{
    
    _backButton.hidden = NO;
    _backButton.alpha = 0.0f;
    
    CGRect googleFrame = _googleSignUpButton.frame;
    CGRect fbFrame = _facebookSignUpButton.frame;
    CGRect signIn = _nativeSignInButton.frame;
    
    [UIView animateWithDuration:.3f animations:^{
        
        _googleSignUpButton.frame = CGRectMake(googleFrame.origin.x, googleFrame.origin.y+24,googleFrame.size.width,googleFrame.size.height);
        _facebookSignUpButton.frame = CGRectMake(fbFrame.origin.x, fbFrame.origin.y+24, fbFrame.size.width, fbFrame.size.height);
        _nativeSignInButton.frame = CGRectMake(signIn.origin.x, signIn.origin.y+24, signIn.size.width, signIn.size.height);
        _nativeSignInButton.titleLabel.text = @"";
        [_nativeSignInButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
        _nativeSignUpButton.alpha = 0.0f;
        _forgotPassword.alpha = 0.0f;
        _backButton.alpha = 1.0f;
        _TitleText.alpha = 0.0f;
        
    } completion:^(BOOL check){
        
        _nativeSignUpButton.hidden = YES;
        _forgotPassword.hidden = YES;
        _emailField.hidden = NO;
        _emailField.alpha = 0.0f;
        _TitleText.text = @"Register";
        
        [UIView animateWithDuration:.3f animations:^{
            _emailField.alpha = 1.0f;
            _TitleText.alpha = 1.0f;
            [_nativeSignInButton setTitle:@"Sign Up" forState:UIControlStateNormal];
            _nativeSignInButton.titleLabel.alpha = 1.0f;
        }];
        
    }];
}



-(void)morphViewForForgot{

        _backButton.hidden = NO;
        _backButton.alpha = 0.0f;
    
    
        CGRect signIn = _nativeSignInButton.frame;
        CGRect email = _emailField.frame;
        email.origin.y = email.origin.y - 20;
        emailL.hidden = NO;
        emailL.alpha = 0.0f;
    
        [UIView animateWithDuration:.3f animations:^{
            
            
            _usernameTF.alpha = 0.0f;
            _passwordTF.alpha = 0.0f;
            _googleSignUpButton.alpha = 0.0f;
            _facebookSignUpButton.alpha = 0.0f;
            _nativeSignInButton.frame = signIn;
            _emailField.frame = CGRectMake(email.origin.x, email.origin.y-20, email.size.width, email.size.height);
            
            _nativeSignInButton.titleLabel.text = @"";
            [_nativeSignInButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
            _nativeSignUpButton.alpha = 0.0f;
            _forgotPassword.alpha = 0.0f;
            _backButton.alpha = 1.0f;
            _TitleText.alpha = 0.0f;
           
            
        } completion:^(BOOL check){
            _usernameTF.hidden = YES;
            _passwordTF.hidden = YES;
            _emailField.hidden = NO;
            _facebookSignUpButton.hidden = YES;
            _googleSignUpButton.hidden = YES;
            _nativeSignUpButton.hidden = YES;
            _forgotPassword.hidden = YES;
            _TitleText.text = @"Recover";
            
            [UIView animateWithDuration:.3f animations:^{
                 emailL.alpha = 1.0f;
                _emailField.alpha = 1.0f;
                _TitleText.alpha = 1.0f;
                [_nativeSignInButton setTitle:@"Send Recovery Email" forState:UIControlStateNormal];
                _nativeSignInButton.titleLabel.alpha = 1.0f;
            }];
            
        }];

}



-(void)defaultEverything{
    _googleSignUpButton.hidden = NO;
    _emailField.hidden = YES;
    _facebookSignUpButton.hidden = NO;
    _usernameTF.hidden= NO;
    _nativeSignInButton.hidden = NO;
    _passwordTF.hidden = NO;
    _nativeSignUpButton.hidden = NO;
    _forgotPassword.hidden = NO;
    _TopSlidePanel.hidden = NO;
    emailL.alpha = 0.3f;
    
    [UIView animateWithDuration:.5f animations:^{
        _TopSlidePanel.frame = defaultTopframe;
        _facebookSignUpButton.frame = defaultFacebook;
        _nativeSignInButton.frame  =defaultSignIn;
        _nativeSignUpButton.frame = defaultSignUp;
        _usernameTF.frame = defaultUsername;
        _passwordTF.frame =defaultPassword;
        _TopSlidePanel.alpha = 1.0f;
        _forgotPassword.alpha = 1.0f;
        _googleSignUpButton.frame = defaultGoogle;
        _googleSignUpButton.alpha = 1.0f;
        _emailField.frame = defaultEmail;
        _facebookSignUpButton.alpha = 1.0f;
        _nativeSignInButton.alpha = 1.0f;
        _nativeSignUpButton.alpha = 1.0f;
        _usernameTF.alpha = 1.0f;
        _passwordTF.alpha = 1.0f;
        _emailField.alpha = 0.0f;
        _backButton.alpha = 0.0f;
        _TitleText.alpha = 0.2f;
        emailL.alpha = 0.0f;

        [_nativeSignInButton setTitle:@"Sign In" forState:UIControlStateNormal];
    } completion:^(BOOL flag){
        _backButton.hidden = YES;
        _emailField.hidden = YES;
        _TitleText.alpha = 1.0f;
        emailL.hidden = YES;
        _TitleText.text = @"Sign In";
    }];
}






//<----------------Boring Delegates--------------------->

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"Welcome"]){
        
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}



@end

//
//  WelcomeViewController.m
//  Rushee
//
//  Created by Michael Giba on 2/28/14.
//  Copyright (c) 2014 Michael Giba. All rights reserved.
//

#import "WelcomeViewController.h"
#import "UIColor+FlatUI.h"
#import <QuartzCore/QuartzCore.h>
#import "FUIButton.h"


@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    self.view.backgroundColor = [UIColor cloudsColor];
    
   
    CGRect window = [[[[UIApplication sharedApplication] delegate] window] bounds];
    CGRect imageRect = CGRectMake(15, window.size.height/2-105, 120, 120);
    CGRect welcomeRect = CGRectMake(window.size.width-305, window.size.height/2-220, 290, 100);

    UILabel *welcomeLabel = [[UILabel alloc] initWithFrame:welcomeRect];
    
    //imageView.backgroundColor = [UIColor redColor];

    
    CALayer *shadow = [CALayer layer];
    CALayer *image = [CALayer layer];
    
    image.frame = imageRect;
    image.cornerRadius = image.frame.size.width/2;
    image.masksToBounds = YES;
    
    UIImage *me = [UIImage imageNamed:@"me.JPG"];
    
    double x = me.size.width/2;
    double y = me.size.height/2;
    double shorterSideLength = MIN(me.size.height, me.size.width);
    x -= shorterSideLength/2;
    y -= shorterSideLength/2;
    
    CGRect crop = CGRectMake(x, y, shorterSideLength, shorterSideLength);
    CGImageRef ref = CGImageCreateWithImageInRect(me.CGImage, crop);
    UIImage *new = [UIImage imageWithCGImage:ref];
    
    
    
    
    image.contents = (id)new.CGImage;
    image.borderWidth = 5.0f;
    image.borderColor = [UIColor cloudsColor].CGColor;
    image.transform = CATransform3DMakeAffineTransform(CGAffineTransformMakeRotation(M_PI));


    
    
    welcomeLabel.text = @"Let's get started";
    welcomeLabel.font = [UIFont systemFontOfSize:30.0];
    welcomeLabel.textAlignment = NSTextAlignmentCenter;
    
 
    CGRect startRect = CGRectMake(45, 340, 230, 45);
    
    FUIButton *start = [[FUIButton alloc] initWithFrame:startRect];
    start.buttonColor = [UIColor amethystColor];
    start.cornerRadius = 2.0f;
    [start setTitle:@"Continue" forState:UIControlStateNormal];
    [start setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    
    
    CGRect nameFrame = CGRectMake(160, 8, 110, 50);
    CGRect cityFrame = CGRectMake(160, 43, 110, 50);
    CGRect countryFrame = CGRectMake(160, 78, 110, 50);

    
    UILabel *NameLabel = [[UILabel alloc] initWithFrame:nameFrame];
    UILabel *countryLabel = [[UILabel alloc] initWithFrame:countryFrame];
    UILabel *cityLabel = [[UILabel alloc] initWithFrame:cityFrame];
    
    NameLabel.text = @"Michael Giba";
    countryLabel.text = @"United States";
    cityLabel.text = @"El Paso";
    
    CGRect infoFrame = CGRectMake(0, 125, 320, 140);
    UIView *info = [[UIView alloc] initWithFrame:infoFrame];
    CALayer *infoShadow = [CALayer layer];
    
    
    infoShadow.frame = infoFrame;
    infoShadow.shadowColor = [UIColor darkGrayColor].CGColor;
    infoShadow.shadowOpacity = 0.5;
    infoShadow.shadowRadius = 2.0f;
    infoShadow.shadowOffset = CGSizeMake(0, 3);
    infoShadow.backgroundColor = [UIColor whiteColor].CGColor;
    
    
    [info addSubview:cityLabel];
    [info addSubview:countryLabel];
    [info addSubview:NameLabel];
    
    
    [self.view addSubview:start];
    [self.view.layer addSublayer:infoShadow];
    [self.view addSubview:info];

    [self.view.layer addSublayer:image];
    [self.view addSubview:welcomeLabel];
    [self.view layoutSubviews];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

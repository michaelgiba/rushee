//
//  SignInViewController.h
//  Rushee
//
//  Created by Michael Giba on 2/24/14.
//  Copyright (c) 2014 Michael Giba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FUIButton.h"
#import "FUITextField.h"
#import <FacebookSDK/FacebookSDK.h>
#import <GooglePlus/GooglePlus.h>




@class GPPSignInButton;

@interface SignInViewController : UIViewController <UITextFieldDelegate, GPPSignInDelegate>

@property (weak, nonatomic) IBOutlet GPPSignInButton *signInButton;

@property (weak, nonatomic) IBOutlet FUIButton *nativeSignInButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookSignUpButton;
@property (weak, nonatomic) IBOutlet UIButton *googleSignUpButton;
@property (weak, nonatomic) IBOutlet FUITextField *usernameTF;
@property (weak, nonatomic) IBOutlet FUITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UIButton *nativeSignUpButton;
@property (weak, nonatomic) IBOutlet UIView *TopSlidePanel;
@property (weak, nonatomic) IBOutlet UIButton *forgotPassword;

- (IBAction)backPushed:(id)sender;
- (IBAction)facebookPressed:(id)sender;
- (IBAction)buttonPushed:(id)sender;
- (IBAction)forgotPressed:(id)sender;
- (IBAction)signUpPressed:(id)sender;






@property (weak, nonatomic) IBOutlet FUIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *TitleText;
@property (weak, nonatomic) IBOutlet UITextField *emailField;

@end

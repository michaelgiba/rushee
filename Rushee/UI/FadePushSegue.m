//
//  FadePushSegue.m
//  Rushee
//
//  Created by Michael Giba on 2/28/14.
//  Copyright (c) 2014 Michael Giba. All rights reserved.
//

#import "FadePushSegue.h"

@implementation FadePushSegue


- (void) perform
{
    CATransition* transition = [CATransition animation];
    
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    
    [[self.sourceViewController navigationController].view.layer addAnimation:transition forKey:kCATransition];
    [[self.sourceViewController navigationController] pushViewController:[self destinationViewController] animated:NO];
}


@end

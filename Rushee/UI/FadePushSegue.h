//
//  FadePushSegue.h
//  Rushee
//
//  Created by Michael Giba on 2/28/14.
//  Copyright (c) 2014 Michael Giba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FadePushSegue : UIStoryboardSegue

@end

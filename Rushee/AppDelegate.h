//
//  AppDelegate.h
//  Rushee
//
//  Created by Michael Giba on 2/24/14.
//  Copyright (c) 2014 Michael Giba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <GooglePlus/GooglePlus.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
